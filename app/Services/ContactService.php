<?php

namespace App\Services;

use App\Contact;


class ContactService
{

	
	public static function findByName(string $name): Contact
	{
		// queries to the db

		$contact = new Contact();

		if($name!='john')
			return $contact;

		$contact->name = "john Doe";
		$contact->number = "5064473366";
		
		return $contact;
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
	}
}