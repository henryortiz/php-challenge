<?php

namespace App;
use App\Interfaces\CarrierInterface;

use App\Call;
use App\Contact;

class Carrier implements CarrierInterface
{
	function __construct()
	{
		# code...
	}

	public function dialContact(Contact $contact)
	{
		
	}

	public function makeCall(): Call
	{
		// some code

        return (new Call() );
	}
}