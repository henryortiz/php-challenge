<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Interfaces\CarrierInterface;
use App\Mobile;
use App\Carrier;
use App\Call;


class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Carrier();

		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_returns_call_when_valid_contact()
	{
		$contactName = 'john doe';
		
		$provider = new Carrier();

		$mobile = new Mobile($provider);

		$this->assertNotNull($mobile->makeCallByName($contactName));
	}

	/** @test */
	public function it_returns_null_when_notexists_contact()
	{
		$contactName = 'maria';
		
		$provider = new Carrier();

		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName($contactName));
	}
}
